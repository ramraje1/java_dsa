import java.io.*;

class ArrayInput{

        public static void main(String[] args)throws IOException{


		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

                char arr[]= new char[5];

                for(int i=0;i<5;i++){

                        System.out.println("Enter "+(i+1)+" Character");
                       arr[i]=(char)(br.read());
		       br.skip(1);
                }

                System.out.println("Print");
                for(int i=0;i<5;i++){

                        System.out.println((i+1)+" Element = "+arr[i]);
                }

        }
}

