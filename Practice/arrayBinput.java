import java.io.*;

class ArrayInput{

        public static void main(String[] args)throws IOException{


		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

                int arr[]= new int[5];

                for(int i=0;i<5;i++){

                        System.out.println("Enter "+(i+1)+" Element");
                       arr[i]=Integer.parseInt(br.readLine());
                }

                System.out.println("Print");
                for(int i=0;i<5;i++){

                        System.out.println((i+1)+" Element = "+arr[i]);
                }

        }
}

