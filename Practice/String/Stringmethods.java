/* public java.lang.String substring(int);
  public java.lang.String substring(int, int);
  public java.lang.CharSequence subSequence(int, int);
  public java.lang.String concat(java.lang.String);
  public java.lang.String replace(char, char);
  public boolean matches(java.lang.String);
  public boolean contains(java.lang.CharSequence);
  public java.lang.String replaceFirst(java.lang.String, java.lang.String);
  public java.lang.String replaceAll(java.lang.String, java.lang.String);
  public java.lang.String replace(java.lang.CharSequence, java.lang.CharSequence);
  public java.lang.String[] split(java.lang.String, int);
  public java.lang.String[] split(java.lang.String);
  public static java.lang.String join(java.lang.CharSequence, java.lang.CharSequence...);
  public static java.lang.String join(java.lang.CharSequence, java.lang.Iterable<? extends java.lang.CharSequence>);
  public java.lang.String toLowerCase(java.util.Locale);
  public java.lang.String toLowerCase();
  public java.lang.String toUpperCase(java.util.Locale);
  public java.lang.String toUpperCase();
*/

import java.util.*;

class StringMethods{

	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);

		System.out.println("Enter First String");
		String str1 = sc.next();

		System.out.println("Enter Second String");
		String str2 = sc.next();

		System.out.println(str1);

	}
}
