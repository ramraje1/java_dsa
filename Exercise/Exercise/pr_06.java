import java.util.*;
class MyReverseStr{

	static String myReverse(String str){

		char arr1[] = str.toCharArray();

		char arr2[] = new char[arr1.length];

		int j=arr1.length;

		for(int i=0;i<arr1.length;i++){

			arr2[i]=arr1[j-1];
			j--;
		}

		return new String(arr2);


	}
	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);

		System.out.println("Enter String");

		String str = sc.nextLine();

		System.out.println(myReverse(str));

	}
}

