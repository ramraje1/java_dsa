import java.util.*;

class Pattern{

	public static void main(String[] args){

		Scanner sc=new Scanner(System.in);

		System.out.println("Enter number of Rows");
		int row=sc.nextInt();

		int ch1=65+row;
		int ch2='a';

		for(int i=0;i<row;i++){

			for(int j=0;j<i;j++){

				System.out.print(" ");

			}

			for(int k=0;k<row-i;k++){


				if(k%2!=0){
				
				
					System.out.print((char)ch2);
					ch2++;
				}else{

					System.out.print((char)ch1);
					ch1--;
				}

			}
			System.out.println();
		}

	}
}
