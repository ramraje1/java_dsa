import java.io.*;

class Pattern{

        public static void main(String[] args) throws IOException{

                BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

                System.out.println("Enter Starting Number  ");
                int start = Integer.parseInt(br.readLine());

		System.out.println("Enter End Number ");
		int end = Integer.parseInt(br.readLine());

		System.out.print("Series =  ");

		for(int i=start;i<=end;i++){

			int count=0;

                        for(int j=1;j<=i;j++){

				if(i%j==0){

					count++;
				}
				if(count>2){

					break;
				}
                        }
			if(count==2){

			         System.out.print(i+" ");

			}
		}
		

		System.out.println();

                
        }
}

