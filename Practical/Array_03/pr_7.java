import java.io.*;

class ArrayDemo{

        public static void main(String[] args)throws IOException{

                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

                System.out.println("Enter Array Size");

                int size = Integer.parseInt(br.readLine());

                int arr[] = new int[size];

                System.out.println("Enter Array Elements");

                for(int i=0;i<arr.length;i++){

                        arr[i] = Integer.parseInt(br.readLine());

                }

		for(int i=0;i<arr.length;i++){
	
				int num=arr[i];
				int temp=num;
				int sum=0;

				while(num!=0){

				 int rem=num%10;

				 int mul=1;

				 for(int j=1;j<=rem;j++){

					 mul=mul*j;
			 	}
				 sum=sum+mul;

				 num=num/10;
				}
			if(sum==temp){

				System.out.println("Strong no "+arr[i]+" found at index : "+i);
			}
		}
        }
}
