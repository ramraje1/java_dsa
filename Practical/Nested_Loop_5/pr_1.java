import java.io.*;

class Pattern{

	public static void main(String[] args) throws IOException{

		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter No of Rows ");

		int row = Integer.parseInt(br.readLine());
		 
		for(int i=1;i<=row;i++){
			int  ch ='A';
			int n=1;

			for(int j=1;j<=row;j++){

				if(i%2!=0){

					ch=ch+row-j;
					n=n+row-j;

					System.out.print((char)ch +""+ n +" ");
				}else{	
					System.out.print((char)ch++ +""+ n++ +" ");
				}
			}
		
			System.out.println();

		}
	}
}
