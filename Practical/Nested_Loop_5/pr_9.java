import java.io.*;

class Pattern{

        public static void main(String[] args) throws IOException{

                BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

                System.out.println("Enter Number ");

                int n = Integer.parseInt(br.readLine());
		int sum=0;

		int temp=n;

		while(n!=0){
			
			int rem=n%10;
			int mult=1;

			for(int i=1;i<=rem;i++){

				mult=mult*i;
			}

			sum=sum+mult;

			n=n/10;

		}

		System.out.println("Addition of factorials of each digit from "+temp+"="+sum);

        }
}

