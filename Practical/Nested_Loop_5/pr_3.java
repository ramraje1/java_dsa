import java.io.*;

class Pattern{

        public static void main(String[] args) throws IOException{

                BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

                System.out.println("Enter No of Rows ");

                int row = Integer.parseInt(br.readLine());

                for(int i=1;i<=row;i++){

			int n = row+(i*2-2);

                        for(int j=1;j<=row-i+1;j++){
				
				System.out.print(n +" ");
				
				n=n-i;
                          
                        }
                        
                        System.out.println();

                }
        }
}

