import java.io.*;

class Pattern{

        public static void main(String[] args) throws IOException{

                BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

                System.out.println("Enter No of Rows ");

                int row = Integer.parseInt(br.readLine());

		int a=0;
		int b=1;
		int sum=0;

                for(int i=1;i<=row;i++){

                        for(int j=1;j<=i;j++){

				if(i==1){
					System.out.print(a+" ");

				}else if(i==2&&j==1){

					System.out.print(b+" ");

			}else{
				sum=a+b;

                                System.out.print(sum +" ");

                                a=b;
				b=sum;
			}

                        }

                        System.out.println();

                }
        }
}


