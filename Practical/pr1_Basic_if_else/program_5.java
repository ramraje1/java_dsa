class Months{

    public static void main(String[] args){

        int month=7;
        
        if(month>=1 && month<=12){

            if(month==1){

                System.out.println("January has a 31 days");
            }else if(month==2){

                System.out.println("February 28 days in common year and 29 days in leap years");
            }else if(month==3){

                System.out.println("March has a 31 days");
            }else if(month==4){

                System.out.println("April has a 30 days");
            }else if(month==5){

                System.out.println("May has a 31 days");
            }else if(month==6){

                System.out.println("June has a 30 days");
            }else if(month==7){

                System.out.println("July has a 31 days");
            }else if(month==8){

                System.out.println("August has a 31 days");
            }else if(month==9){

                System.out.println("September has a 30 days");
            }else if(month==10){

                System.out.println("October has a 31 days");
            }else if(month==11){

                System.out.println("November has a 30 days");
            }else if(month==12){

                System.out.println("December has a 31 days");
            }

        }else{

            System.out.println("Invalid Months");
        }
    }
}