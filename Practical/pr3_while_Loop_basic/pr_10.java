class Palindrome{

	public static void main(String[] args){

		int num=2332;
		int rev=0;
		int temp=num;

		while(num!=0){

			rev=rev*10+(num%10);

			num=num/10;
		}

		if(rev==temp){

			System.out.println("The given number is Palindrome ");

		}else{

			System.out.println("The given number Not palindrome");
		}
	}
}
