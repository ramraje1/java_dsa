class Strong{

	public static void main(String[] args){

		int N=145;
		int temp=N;
		int sum=0;

		while(N!=0){

			int rem=N%10;
			int mult=1;

			for(int i=1;i<=rem;i++){

				mult=mult*i;
			}

			sum=sum+mult;

			N=N/10;
		}

		if(sum==temp){

			System.out.println("Strong Number");
		}else{

			System.out.println("Not Strong Number");
		}
	}
}
