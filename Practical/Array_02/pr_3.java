import java.io.*;

class ArrayDemo{
        
        public static void main(String[] args)throws IOException{
                
                BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
                
                System.out.println("Enter Array size");
                
                int size=Integer.parseInt(br.readLine());
                
                int arr[]=new int[size];
                
                System.out.println("Enter elements in Array");
                
		int evensum=0;
		int oddsum=0;
                for(int i=0;i<arr.length;i++){
                        
                        arr[i]=Integer.parseInt(br.readLine());
                        
			if(arr[i]%2==0){

				evensum=evensum+arr[i];
			}else{
				oddsum=oddsum+arr[i];
			}
                }       
                
		System.out.println("Sum of Even Elements : "+evensum);
		System.out.println("Sum of Odd Elements  : "+oddsum);
        }       
} 
