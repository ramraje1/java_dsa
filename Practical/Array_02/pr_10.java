import java.io.*;

class ArrayDemo{

        public static void main(String[] args)throws IOException{

                BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

                System.out.println("Enter Array size");

                int size=Integer.parseInt(br.readLine());

                int arr[]=new int[size];

                System.out.println("Enter  array elements");

             	   for(int i=0;i<arr.length;i++){

                	        arr[i]=Integer.parseInt(br.readLine());

			}

		   System.out.println("Output : ");

			for(int j=0;j<arr.length;j++){
	
				int temp=arr[j];
				int sum=0;
			
				while(temp!=0)
				{
					int rem=temp%10;
					sum=sum+rem;	

					temp=temp/10;
				}
				if(sum%2==0){
					System.out.println(arr[j]);
				
				
			}
		}	
	}
}
