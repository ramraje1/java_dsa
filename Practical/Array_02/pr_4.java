import java.io.*;

class ArrayDemo{
        
        public static void main(String[] args)throws IOException{
                
                BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
                
                System.out.println("Enter Array size");
                
                int size=Integer.parseInt(br.readLine());
                
                int arr[]=new int[size];
                
                System.out.println("Enter elements in Array");
                
                for(int i=0;i<arr.length;i++){
                        
                        arr[i]=Integer.parseInt(br.readLine());
                        
                }

		System.out.println("Enter element to search : ");

		int sc=Integer.parseInt(br.readLine());

		int sccount=0;

		for(int i=0;i<arr.length;i++){

			sccount++;

			if(arr[i]==sc){
	
				System.out.println("Element found at index : "+(sccount-1));		

			}
		}

                
        }       
} 
