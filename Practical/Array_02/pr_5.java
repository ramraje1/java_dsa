import java.io.*;

class ArrayDemo{

        public static void main(String[] args)throws IOException{

                BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

                System.out.println("Enter Array size");

                int size=Integer.parseInt(br.readLine());

                int arr[]=new int[size];

                System.out.println("Enter elements in Array");

                for(int i=0;i<arr.length;i++){

                        arr[i]=Integer.parseInt(br.readLine());

                }

		int min=arr[0];

		for(int i=0;i<arr.length;i++){

			if(min>arr[i]){

				min=arr[i];
			}
		}

		System.out.println("min element : "+min);

        }
}

