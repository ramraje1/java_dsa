import java.util.*;

class ArrayDemo{

	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);

		System.out.println("Enter array size");

		int size=sc.nextInt();

		int arr[]=new int[size];

		System.out.println("Enter Array Elements");

		int mult=1;

		for(int i=0;i<arr.length;i++){

			arr[i]=sc.nextInt();

			if(arr[i]%2==0){

				mult=mult*arr[i];
			}
		}

		System.out.println("Product of even elements = "+mult);

	}
}
