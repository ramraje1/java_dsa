import java.util.*;

class ArrayDemo{

	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);

		System.out.println("Enter array size");

		int size=sc.nextInt();

		int arr[]=new int[size];

		System.out.println("Enter Array Elements");

		for(int i=0;i<arr.length;i++){

			arr[i]=sc.nextInt();
		}

		for(int i=0;i<arr.length;i++){
		
			if(arr[i]%5==0){
			
				System.out.print(arr[i]+"  ");
			}
		}
		System.out.println();

	}
}
