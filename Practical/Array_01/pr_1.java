import java.util.*;

class ArrayDemo{
	
	public static void main(String[] args){

	Scanner sc=new Scanner(System.in);

	System.out.println("Enter Array size");

	int size=sc.nextInt();

	int arr[]=new int[size];

	System.out.println("Enter array elements");

	int sum=0;
	for(int i=0;i<arr.length;i++){

		arr[i]=sc.nextInt();

		if(arr[i]%2!=0){

			sum=sum+arr[i];
		}
	}
	System.out.println("Sum of Odd elements = "+sum);
	}
}

