class Nested{

	public static void main(String[] args){

		int n=4;

		int num=(n*(n+1))/2;

		int ch='A'+((n*(n+1))/2)-1;

		for(int i=1;i<=n;i++){

			for(int j=1;j<=i;j++){

				if(i%2==1){

					System.out.print(num +" ");
				}else{
					System.out.print((char)(ch)+" ");
				}
				num--;
				ch--;
			}
			System.out.println();
		}
	}
}
