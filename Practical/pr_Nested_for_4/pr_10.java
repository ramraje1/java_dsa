class Nested{

	public static void main(String[] args){

		int n=4;

		for(int i=1;i<=n;i++){

			int num=i;

			for(int j=1;j<=i;j++){

				if(num%2==0&&j%2==1){

					System.out.print((num*num*num)+" ");
				}else{

					System.out.print((num*num)+" ");
				}
				num++;
			}
			System.out.println();
		}
	}
}

