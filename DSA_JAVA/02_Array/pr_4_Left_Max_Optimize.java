class LeftMax{
	public static void main(String[] args){

		int arr[] = new int[]{-3,6,2,4,5,2,8,-9,3,1};
		int n=10;
		int Leftmax[]=new int[n];
		Leftmax[0]=arr[0];

		for(int i=1;i<n;i++){

			if(Leftmax[i-1]<arr[i]){

				Leftmax[i]=arr[i];
			}else{
				Leftmax[i]=Leftmax[i-1];
			}
		}

		for(int i=0;i<n;i++){

			System.out.println(Leftmax[i]+"  ");
		}
	}
}
