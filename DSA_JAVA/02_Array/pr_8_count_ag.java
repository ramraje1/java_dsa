public class pr_8_count_ag {

    public static void main(String[] args) {
        char arr[] = new char[] { 'b', 'a', 'a', 'a', 'c', 'g', 'g', 'g' };
        int n = 8;
        int pair = 0;
        int count = 0;

        for (int i = 0; i < n; i++) {
            if (arr[i] == 'a') {
                count++;
            } else if (arr[i] == 'g') {
                pair = pair + count;
            }
        }
        System.out.println(count);
        System.out.println(pair);
    }
}
