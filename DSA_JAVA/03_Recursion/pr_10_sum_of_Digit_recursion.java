class sumofDigit{

	int sum(int num){

		int sum=0;

		while(num>0){

			sum=num%10+sum;

			num=num/10;
		}
		return sum;
	}

	public static void main(String[] args){

		sumofDigit obj=new sumofDigit();

		System.out.println(obj.sum(123));
		
	}
}
