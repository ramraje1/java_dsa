class DoubleRecursion{

	int fun(int num){

		if(num<=1)
			return 1;

		return fun(num-2)+fun(num-1);
	}
	public static void main(String[] args){

		DoubleRecursion obj =new DoubleRecursion();

		System.out.println(obj.fun(5));
	}
}
