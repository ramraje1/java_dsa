class SumOfN{

        int fun(int num){

                if(num==1)
			return 1;

		return num+fun(--num);
        }

        public static void main(String[] args){

                System.out.println("Start main");

                SumOfN obj=new SumOfN();

                int ret=obj.fun(5);
		System.out.println(ret);

                System.out.println("End main");
        }
}
