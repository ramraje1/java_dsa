import java.io.*;

class InputDemo{

        public static void main(String[] args) throws IOException{

                InputStreamReader isr=new InputStreamReader(System.in);

                BufferedReader br=new BufferedReader(isr);

                System.out.println("Enter Batsman name");
		String name1=br.readLine();

		System.out.println("Enter Bowler Name");
		String name2=br.readLine();

		System.out.println("Batsman name is " +name1);
		System.out.println("Bowler name is " +name2);
	}
}

