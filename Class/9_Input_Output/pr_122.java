import java.io.*;

class InputDemo{

        public static void main(String[] args) throws IOException{

                BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

                System.out.println("Enter Building  name");
                String name=br.readLine();

		System.out.println("Enter Wing : ");
		char wing=(char)br.read();

		br.skip(1);

		System.out.println("Enter Flat Number");
		int flat=Integer.parseInt(br.readLine());

		System.out.println("Building name is : "+ name);
		System.out.println("Wing : "+wing);
		System.out.println("Flat Number is : "+ flat);
	}
}

