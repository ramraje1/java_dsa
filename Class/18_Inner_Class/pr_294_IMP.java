class Demo{

	Demo(){

		System.out.println("In Demo Constructor");
	}
}
class Demochild extends Demo{

	Demochild(){

		System.out.println("In Demochild Constructor");
	}
}
class Parent{

	Parent(){

		System.out.println("In Parent Constructor");
	}
	Demo m1(){

		System.out.println("In m1 Parent");
		return new Demo();
	}
}
class child extends Parent{

	child(){
		System.out.println("In child Constructor");
	}

	Demochild m1(){

		System.out.println("In m1 child");
		return new Demochild();
	}
}
class client {

	public static void main(String[] args){

		Parent p = new child();
		p.m1();
	}
}
