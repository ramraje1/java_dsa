class Parent {

	int x=10;
	static int y =20;

	Parent (){

		System.out.println("In Parent Constructor");
	}
	void m1(){

		System.out.println("In Parent m1");
	}
	static void m2(){

		System.out.println("In Parent m2");
	}
}
class child extends Parent{

	int a=10;
	static int b=20;

	child(){

		System.out.println("In child Constructor");
	}
	void m1(){

		System.out.println("In child m1");
	}
	static void m3(){

		System.out.println("In child m3");
	}
}
class client {

	public static void main (String[] args){

		Parent obj=new Parent();
		obj.m1();
		Parent.m2();

		Parent obj1 =new child();
		obj1.m1();
		child.m3();

		child obj2=new child();
		obj2.m1();
		obj2.m3();
	}
}
