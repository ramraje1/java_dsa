class MethodChaining{

	public static void main(String[] args){

		StringBuffer sb =new StringBuffer("Shashi");

		System.out.println(sb);

		String str1="Core2web";

		System.out.println(System.identityHashCode(str1));

		StringBuffer sb2 =new StringBuffer(str1);

		str1=sb2.reverse().toString();

		System.out.println(str1);

		System.out.println(System.identityHashCode(str1));

	}
}
