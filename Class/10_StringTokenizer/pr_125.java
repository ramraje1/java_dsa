import java.io.*;
import java.util.*;

class Tokenizer{

	public static void main(String[] args) throws IOException{

		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter Socity name, wing, flatno");

		String info=br.readLine();
		System.out.println(info);

		StringTokenizer obj = new StringTokenizer(info,",");

		String token1 = obj.nextToken();
		String token2 = obj.nextToken();
		String token3 = obj.nextToken();

		System.out.println("Socity = "+token1);
		System.out.println("Wing = "+token2);
		System.out.println("Flat = "+token3);

	}
}

