import java.io.*;
import java.util.*;

class Tokenizer{

	public static void main(String[] args) throws IOException{

		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter hostel name, Floar(A,B,C,D),RoomNo,Height");

		String info=br.readLine();

		System.out.println(info);

		StringTokenizer obj = new StringTokenizer(info,",");

		String Name = obj.nextToken();
		char Floar = obj.nextToken().charAt(0);
		int RoomNo =Integer.parseInt(obj.nextToken());
		float Height=Float.parseFloat(obj.nextToken());

		System.out.println("Hostel Name = "+Name);
		System.out.println("Floar = "+Floar);
		System.out.println("RoomNo = "+RoomNo);
		System.out.println("Height = "+Height);

	}
}

