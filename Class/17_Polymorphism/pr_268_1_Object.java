class Demo{

	void fun(String str){

		System.out.println("String");
	}
	void fun(StringBuffer str1){

		System.out.println("StringBuffer");
	}
}
class Client{

	public static void main(String[] args){

		Demo obj = new Demo();
		obj.fun("Core2web");  //String
		obj.fun(new StringBuffer("Core2web"));   //StringBuffer
		obj.fun(null);          //Ambiquity 
	}
}
