class Parent{

        Parent(){

                System.out.println("Parent Constructor");
        }
        void fun(){

                System.out.println("In Fun");
        }
}

class Child extends Parent{

        Child(){
                System.out.println("Child Constructor");
        }
        void gun(){

                System.out.println("In gun");
        }
}
class Client{

        public static void main(String[] args){

		Parent obj1 = new Child();
                Child obj2 = new Child();

                obj1.fun();
	}
}

