abstract class Deffence{

	void security(){

		System.out.println("Security");
	}
	abstract void budget();
}
class Army extends Deffence{

	void budget(){

		System.out.println("1124 cr");
	}
}
class Client{

	public static void main(String[] args){

		Deffence obj = new Army();
		obj.security();
		obj.budget();
	}
}
