import java.io.*;

class FileDemo{

        public static void main(String[] args)throws IOException{

                File fobj=new File("Incubetor.txt");

                fobj.createNewFile();

		System.out.println(fobj.getName());
		System.out.println(fobj.getParent());
		System.out.println(fobj.getPath());
		System.out.println(fobj.getAbsolutePath());

		System.out.println(fobj.canRead());
		System.out.println(fobj.canWrite());
		System.out.println(fobj.isDirectory());
		System.out.println(fobj.isFile());
		System.out.println(fobj.isHidden());

		System.out.println(fobj.lastModified());
		System.out.println(fobj.list());

        }
}

