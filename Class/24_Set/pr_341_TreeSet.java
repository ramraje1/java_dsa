import java.util.*;

class TreeSetDemo{

	public static void main(String[] args){

		TreeSet ts = new TreeSet();

		ts.add("Kanha");
		ts.add("Ashish");
		ts.add("Krushna");
		ts.add("Akash");
		ts.add("Ramraje");

		System.out.println(ts);
	}
}
