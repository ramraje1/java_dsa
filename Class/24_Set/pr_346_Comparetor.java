import java.util.*;

class Movies{

	String movieName=null;
	double totColl =0.0;
	float imdRating = 0.0f;

	Movies(String movieName,double totColl,float imdRating){

		this.movieName = movieName;
		this.totColl = totColl;
		this.imdRating = imdRating;

	}
	public String toString(){

		return "{"+movieName+" , "+totColl+" , "+imdRating+" }";
	}
}
class SortByName implements Comparator{

	public int compare(Object obj1,Object obj2){

		return ((Movies)obj1).movieName.compareTo(((Movies)obj2).movieName);
	}
}
class SortByColl implements Comparator{

	public int compare(Object obj1,Object obj2){

		return (int)((((Movies)obj1).totColl)-(((Movies)obj2).totColl));
	}
}
class SortByIMDB implements Comparator{

        public int compare(Object obj1,Object obj2){

                return (int)((((Movies)obj1).imdRating)-(((Movies)obj2).imdRating));
        }
}
class UserListSort{

	public static void main(String[] args){

		ArrayList al = new ArrayList();

		al.add(new Movies("RHTM",200.00,8.8f));
		al.add(new Movies("Ved",700.00,9.8f));
		al.add(new Movies("Sairat",900.00,8.0f));
		al.add(new Movies("Bajrangi",100.00,6.8f));

		System.out.println(al);

		Collections.sort(al,new SortByName());
		System.out.println(al);

		Collections.sort(al,new SortByColl());
                System.out.println(al);

		Collections.sort(al,new SortByIMDB());
                System.out.println(al);

	}
}

