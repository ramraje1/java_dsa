import java.util.*;

class Movies implements Comparable{

	String movieName=null;
	float totColl = 0.0f;

	Movies(String movieName,float totColl){

		this.movieName = movieName;
		this.totColl = totColl;
	}
	public int compareTo(Object obj){

		return -(movieName.compareTo(((Movies)obj).movieName));
	}
	public String toString(){

		return movieName;
	}
}
class TreeSetDemo{

	public static void main(String[] args){

		TreeSet ts = new TreeSet();

		ts.add(new Movies("Gadar2",134.0f));
		ts.add(new Movies("OMG2",50.0f));
		ts.add(new Movies("jailer",150.0f));
		ts.add(new Movies("OMG2",50.0f));

		System.out.println(ts);
	}
}
