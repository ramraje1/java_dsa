import java.util.*;

class SortedSetDemo{

	public static void main(String[] args){

		SortedSet ss = new TreeSet();

		ss.add("Kanha");
		ss.add("Ramraje");
		ss.add("Akash");
		ss.add("Krushna");
		ss.add("Bakle");

		System.out.println(ss);

		System.out.println(ss.headSet("Ramraje"));

		System.out.println(ss.headSet("Kanha"));

		System.out.println(ss.tailSet("Kanha"));

		System.out.println(ss.subSet("Bakle","Ramraje"));

		System.out.println(ss.first());

		System.out.println(ss.last());

		System.out.println(ss);
	}
}
