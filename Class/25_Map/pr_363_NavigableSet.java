import java.util.*;

class NavigableSetDemo{

	public static void main(String[] args){

		NavigableSet ns = new TreeSet();

		ns.add("Kanha");
		ns.add("Ramraje");
		ns.add("Akash");
		ns.add("Krushna");
		ns.add("Bakle");

		System.out.println(ns);

		System.out.println(ns.lower("Kanha"));
		System.out.println(ns.floor("Kanha"));
		System.out.println(ns.ceiling("Kanha"));
		System.out.println(ns);
	}
}
