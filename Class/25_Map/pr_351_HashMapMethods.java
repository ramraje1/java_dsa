import java.util.*;

class HashMapMethods{

	public static void main(String[] args){

		HashMap hm = new HashMap();

		hm.put("Java",".java");
		hm.put("Cpp",".cpp");
		hm.put("Dart",".dart");
		hm.put("Python",".py");

		System.out.println(hm);

		//get
		System.out.println(hm.get("Java"));

		//keySet
		System.out.println(hm.keySet());

		//values
		System.out.println(hm.values());

		//entrySet
		System.out.println(hm.entrySet());

	}
}
