import java.util.*;

class SortedMapDemo{

        public static void main(String[] args){

                SortedMap tm = new TreeMap();

                tm.put("Ind","India");
                tm.put("Pak","Pakistan");
                tm.put("SL","Srilanka");
                tm.put("Aus","Australia");
                tm.put("Ban","Bangladesh");

                System.out.println(tm);

		//subMap
		System.out.println("subMap = "+tm.subMap("Aus","Pak"));

		//headMap
		System.out.println("headMap = "+tm.headMap("Pak"));

		//tailMap
		System.out.println("tailMap = "+tm.tailMap("Pak"));

		//firstKey
		System.out.println("firstKey = "+tm.firstKey());

		//lastKey
		System.out.println("lastKey = "+tm.lastKey());

		//keySet
		System.out.println("keySet = "+tm.keySet());

		//values
		System.out.println("values = "+tm.values());

		//entrySet
		System.out.println("entrySet = "+tm.entrySet());
        }
}

