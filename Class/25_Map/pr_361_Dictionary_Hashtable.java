import java.util.*;

class DictionaryDemo{
        
        public static void main(String[] args){
                
                Dictionary ht = new Hashtable();
                
                ht.put(10,"Sachin");
                ht.put(7,"MSD");
                ht.put(45,"Rohit");
                ht.put(18,"Virat");
                ht.put(1,"KLRahul");
                
                System.out.println(ht);

		//keys
		Enumeration itr =ht.keys();
		while(itr.hasMoreElements()){

			System.out.println(itr.nextElement());
		}
		//elements
		Enumeration itr1 = ht.elements();
		while(itr1.hasMoreElements()){

			System.out.println(itr1.nextElement());
		}

		System.out.println(ht.get(10));

		ht.remove(1);

		System.out.println(ht);
        }       
}
