class Demo{

	Demo(){

		System.out.println("No Arg");

	}
	Demo(int x){

		System.out.println(x);
		System.out.println("Parameter ");
	}
	Demo(Demo xys){

		System.out.println("Parameter Demo");

	}
	public static void main(String[] args){

		Demo obj1=new Demo();
		Demo obj2=new Demo(10);
		Demo obj3=new Demo(obj2);
	}
}

