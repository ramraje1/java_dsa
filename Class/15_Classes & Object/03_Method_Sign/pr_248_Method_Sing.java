class Demo{

        int x=10;

        Demo(){           //Demo(Demo this)

	//	this(10);
                System.out.println("In No args Constructer");
         }
        Demo(int x){      //Demo(Demo this,int x)

		this();
                System.out.println("In Para Constructer");
         }

        public static void main(String[] args){

                Demo obj1=new Demo();   //Demo(obj1)
                Demo obj2=new Demo(10);  //Demo(obj2)

        }
}

