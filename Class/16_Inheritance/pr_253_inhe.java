class Parent{

	Parent(){

		System.out.println("In Parent Constructer");
	}

	void parentProperty(){

		System.out.println("Flat,car,Gold");

	}
}

class Child extends Parent{

	Child(){

		System.out.println("In Child Constructer");

	}
}
class Client{

	public static void main(String[] args){

		Child obj = new Child();

		obj.parentProperty();
	}
}
