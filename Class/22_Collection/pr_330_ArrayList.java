import  java.util.*;

class ArrayListDemo extends ArrayList{

	public static void main(String[] args){

		ArrayList al = new ArrayList();

		//add(Elements)
		al.add(10);
		al.add(20.5f);
		al.add("Ramraje");
		al.add(10);
		al.add(20.5f);

		System.out.println(al);

		// int size()

		System.out.println(al.size());

		//boolean contains(Object)

		System.out.println(al.contains("Ramraje"));
		System.out.println(al.contains("30"));

		// int indexOf(Object)
		System.out.println(al.indexOf("Ramraje"));
		System.out.println(al.indexOf(10));

		//int lastIndexOf(Object)

		System.out.println(al.lastIndexOf(10));
		System.out.println(al.lastIndexOf(20.5f));

		// E get(int ) int--index
		System.out.println(al.get(2));

		//E set(int,E) 
		System.out.println(al.set(2,"RAMRAJE"));
		System.out.println(al);

		//void add(int , E)
		al.add(2,"Bakle");
		System.out.println(al);

		//E remove(int)

		System.out.println(al.remove(2));

		//boolean remove(Object)

		System.out.println(al.remove("Bakle"));
		System.out.println(al);

		ArrayListDemo al2=new ArrayListDemo();

		al2.add("Ram");
		al2.add("Raje");
		al2.add("Shashi");

		//boolean addAll(Collection);

		al.addAll(al2);
		System.out.println(al);

		//boolean addAll(int,collection)

		al.addAll(3,al2);
		System.out.println(al);

		//protecte void removeRange(int,int)

		al2.removeRange(1,2);
		System.out.println(al2);

		//java.lang.Object[] toArray();

		Object arr[]=al.toArray();

		System.out.println(arr[0]);

		for(Object data:arr){

			System.out.println(data);
		}
		System.out.println(al);

		//void clear()

		al.clear();
		System.out.println(al);
	}
}


