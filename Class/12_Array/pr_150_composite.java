import java.util.*;

class ArrayDemo{

	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);

		System.out.println("Enter Array Size");

		int size=sc.nextInt();

		int arr[]=new int[size];

		System.out.println("Enter Array elements");

		for(int i=0;i<arr.length;i++){

			arr[i]=sc.nextInt();
		}

		for(int j=0;j<arr.length;j++){

			int num=arr[j];

			int count=0;
			boolean flag=false;

			for(int i=1;i<=num;i++){

				if(num%i==0){

					count++;

					if(count>2){

						flag=true;
						break;
					}
				}
			}
			if(flag==true){

				System.out.println(arr[j]+"  Composite Number");
			}
		}

	}
}
