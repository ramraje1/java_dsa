import java.io.*;

class ArrayDemo{

	public static void main(String[] args)throws IOException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter Array Size");

		int size = Integer.parseInt(br.readLine());

		int arr[] = new int[size];

		System.out.println("Enter Array Elements");

		for(int i=0;i<arr.length;i++){

			arr[i] = Integer.parseInt(br.readLine());

		}

		for(int j=0;j<arr.length;j++){

			int num = arr[j];
			int rev=0;

			while(num!=0){

				int rem=num%10;

				rev=rev*10+rem;

				num=num/10;
			}
			System.out.println(rev);
		}
	}
}
