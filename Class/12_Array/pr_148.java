import java.util.*;

class ArrayDemo{

	public static void main(String[] args){

		Scanner sc=new Scanner(System.in);

		System.out.println("Enter Array size");

		int size=sc.nextInt();

		int arr[]=new int[size];

		System.out.println("Enter Elements");

		int evencount=0;
		int oddcount=0;

		for(int i=0;i<arr.length;i++){

			arr[i]=sc.nextInt();

			if(arr[i]%2==0){

				evencount++;
			}else{

				oddcount++;
			}
		}

		System.out.println("Even Count = "+evencount);
		System.out.println("Odd  Count = "+oddcount);
	}
}


