import java.io.*;

class ArrayDemo{

	public static void main(String[] args)throws IOException{

		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		int sum=0;

		int arr[] = new int[5];

		System.out.println("Enter values");

		for(int i=0;i<5;i++){

			arr[i]=Integer.parseInt(br.readLine());

		}

		for(int i=0;i<5;i++){

			sum=sum+arr[i];
		}
		System.out.println("Array sum = "+sum);
	}
}
