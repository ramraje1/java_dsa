import java.util.*;

class BallInOverLimitException extends RuntimeException{

	BallInOverLimitException(String msg){

		super(msg);
	}
}
class Match{

	public static void main(String[] args){

		int arr[] = new int[6];

		Scanner sc= new Scanner(System.in);

		System.out.println("Over : Balls");

		for(int i=0;i<arr.length;i++){

			int Ball=sc.nextInt();

			if(Ball>6) throw new BallInOverLimitException("Mitra over madhe 6 ball astat");

		}
		for(int i=0;i<arr.length;i++){

			System.out.println(arr[i]);
		}
	}
}
