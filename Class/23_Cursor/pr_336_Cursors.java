import java.util.*;

class CursorDemo{

	public static void main(String[] args){

		ArrayList al = new ArrayList();
		al.add(10);
		al.add(20.5);
		al.add(30.5f);
		al.add("Ramraje");

		for(var x:al){

			System.out.println(x.getClass().getName());
		}

		//iterator

		Iterator cursor = al.iterator();

		while(cursor.hasNext()){
			if("Ramraje".equals(cursor.next())){

				cursor.remove();
			}
		}
		System.out.println(al);

		//ListIterator

		ListIterator liter=al.listIterator();

		while(liter.hasNext()){

			System.out.println(liter.next());
		}
		while(liter.hasPrevious()){

			System.out.println(liter.previous());
		}

		//Enumeration

		Vector v = new Vector();
		v.add("Ramraje");
		v.add("Ashish");
		v.add("Akash");
		v.add("Krushna");

		Enumeration cursor1 = v.elements();

		System.out.println(cursor1.getClass().getName());

		while(cursor1.hasMoreElements()){

				System.out.println(cursor1.nextElement());
		}
	}
}
