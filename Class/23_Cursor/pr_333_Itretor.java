import java.util.*;

class IteratorDemo{

	public static void main(String[] args){

		ArrayList al = new ArrayList();

		al.add("Ramraje");
		al.add("Akash");
		al.add("Krushna");

		Iterator itr = al.iterator();

		while(itr.hasNext()){

			if("Ramraje".equals(itr.next()))
				itr.remove();

			System.out.println(itr.next());
		}
		System.out.println(al);
	}
}
