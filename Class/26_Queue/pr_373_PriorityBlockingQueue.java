import java.util.concurrent.*;
import java.util.*;

class PriorityBlockingDemo{

	public static void main(String[] args){
	BlockingQueue bQueue = new PriorityBlockingQueue();

	bQueue.offer(10);
	bQueue.offer(30);
	bQueue.offer(20);
	bQueue.offer(40);
	bQueue.offer(50);
	bQueue.add(60);
	bQueue.poll();

	System.out.println(bQueue.remainingCapacity());
	}
}
