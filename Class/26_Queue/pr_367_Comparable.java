import java.util.*;
class Project implements Comparable{

	String pname=null;
	int teamSize =0;
	int duration=0;

	Project(String pname,int teamSize,int duration){

		this.pname = pname;
		this.teamSize=teamSize;
		this.duration=duration;
	}
	public String toString(){

		return pname+" : "+teamSize+" : "+duration;
	}
	 public int compareTo(Object obj){

		 return pname.compareTo(((Project)obj).pname);
	 }
}
class ProjectDemo{

	public static void main(String[] args){

		TreeSet ts=new TreeSet();

		ts.add(new Project("Whatsapp",10,20));
		ts.add(new Project("Instagram",3,40));
		ts.add(new Project("Facebook",17,20));

		System.out.println(ts);
	}
}
