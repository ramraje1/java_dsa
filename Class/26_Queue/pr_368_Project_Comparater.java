import java.util.*;
class Project{

	String proName=null;
	int teamSize=0;
	int duration=0;

	Project(String proName,int teamSize,int duration){

		this.proName=proName;
		this.teamSize=teamSize;
		this.duration=duration;
	}
	public String toString(){

		return proName+" : "+teamSize+" : "+duration;

	}
}
class SortByName implements Comparator<Project>{

	public int compare(Project obj1,Project obj2){

		return obj1.proName.compareTo(obj2.proName);
	}
}
class SortByteamSize implements Comparator <Project>{

	public int compare(Project obj1,Project obj2){

		return (int)obj1.teamSize-(obj2.teamSize);
	}
}
class SortByduration implements Comparator <Project>{

        public int compare(Project obj1,Project obj2){

                return (int)obj1.duration-(obj2.duration);
        }
}
class ProjectDemo{

	public static void main(String[] args){

		ArrayList<Project> ts = new ArrayList<Project>();

		ts.add(new Project("Whatsapp",2,18));
		ts.add(new Project("Facebook",5,12));
		ts.add(new Project("Instagram",50,9));

		System.out.println(ts);

		Collections.sort(ts,new SortByName());
		System.out.println(ts);

		Collections.sort(ts,new SortByteamSize());
                System.out.println(ts);


		Collections.sort(ts,new SortByduration());
                System.out.println(ts);

	}
}
