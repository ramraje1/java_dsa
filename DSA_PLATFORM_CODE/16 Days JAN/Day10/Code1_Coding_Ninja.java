import java.util.* ;
import java.io.*; 
import java.util.ArrayList;

public class Solution {
	public static ArrayList<Integer> sqsorted(ArrayList<Integer> arr) {
		for (int i = 0; i < arr.size(); i++) {
            int squared = arr.get(i) * arr.get(i);
            arr.set(i, squared);
        }

        Collections.sort(arr);

        return arr;
	}
}