class Solution {
    public int[][] construct2DArray(int[] original, int m, int n) {
        int totalElements = m * n;
        
        if (original.length != totalElements) {
 
            return new int[0][0]; 
        }

        int[][] result = new int[m][n];

        for (int i = 0; i < totalElements; i++) {
            result[i / n][i % n] = original[i];
        }

        return result;
    }
}