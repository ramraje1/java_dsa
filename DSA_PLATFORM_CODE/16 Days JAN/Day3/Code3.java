//{ Driver Code Starts
//Initial Template for Java

import java.io.*;
import java.util.*;

public class Code3 {

    public static void main(String[] args) throws Exception {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        int tc = Integer.parseInt(br.readLine().trim());
        while (tc-- > 0) {
            String[] inputLine;
            inputLine = br.readLine().trim().split(" ");
            int n = Integer.parseInt(inputLine[0]);
            int k = Integer.parseInt(inputLine[1]);
            int[] arr = new int[n];
            inputLine = br.readLine().trim().split(" ");
            for (int i = 0; i < n; i++) {
                arr[i] = Integer.parseInt(inputLine[i]);
            }
            int ans = new Solution().getPairsCount(arr, n, k);
            System.out.println(ans);
        }
    }
}

// } Driver Code Ends


//User function Template for Java

class Solution {
    int getPairsCount(int[] arr, int n, int k) {
        // code here
        
        
         int[] array = new int[k+1]; 
         int count=0;
     
        for(int i=0;i<n;i++){
           
             int j = k-arr[i];
            
             if(j>0 && array[j]!=0){
                  
                   count=count+array[j];
                }
         
             if(arr[i]<k){
             
                 array[arr[i]]++;
                
                  }
            } 
            
            return count;
            
            
            
            
       /* int count =0;
        for(int i=0;i<n-1;i++){
            
            for(int j=i+1;j<n;j++){
                
                if(arr[i]+arr[j]==k){
                    
                    count++;
                }
            }
        }
        
        return count;*/
        
    }
}
