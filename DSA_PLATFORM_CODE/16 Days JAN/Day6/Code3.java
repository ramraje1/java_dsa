import java.io.*;
import java.util.*;
class GfG
{
    public static void main (String[] args)
    {
        
        Scanner sc = new Scanner(System.in);
        int t = sc.nextInt();
        
        while(t-- > 0)
        {
            int n = sc.nextInt();
            ArrayList<Integer> arr = new ArrayList<>();
            for(int i = 0;i<n;i++)
                {
                    int x = sc.nextInt();
                    arr.add(x);
                }
            int m = sc.nextInt();
            
            Solution ob = new Solution();
            
    		System.out.println(ob.findMinDiff(arr,n,m));
        }
        
    }
}
// } Driver Code Ends
//User function Template for Java

class Solution
{
    public long findMinDiff (ArrayList<Integer> A, int N, int M)
    {
        if (N < M) {
            return -1;
        }
        
        Collections.sort(A);
        int minDifference = Integer.MAX_VALUE;

        for (int i = 0; i + M - 1 < N; i++) {
            int difference = A.get(i + M - 1) - A.get(i);
            minDifference = Math.min(minDifference, difference);
        }

        return minDifference;
    }
}