class Solution {
    public int countQuadruplets(int[] nums) {
       int count = 0;
        int[] m = new int[201];
        for (int i = 1; i < nums.length - 2; i++) {
            for (int j = 0; j < i; j++) {
                m[nums[j] + nums[i]]++;
            }
            for (int j = i + 2; j < nums.length; j++) {
                int diff = nums[j] - nums[i + 1];
                if (diff >= 0) {
                    count += m[diff];
                }
            }
        }
        return count;
    }
}