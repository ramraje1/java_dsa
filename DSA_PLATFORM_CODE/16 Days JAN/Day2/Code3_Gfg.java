import java.util.*;
import java.lang.*;
import java.io.*;

class GFG {
	public static void main(String[] args) throws IOException
	{
	        BufferedReader br =
            new BufferedReader(new InputStreamReader(System.in));
        int t =
            Integer.parseInt(br.readLine().trim()); // Inputting the testcases
        while(t-->0)
        {
            long n = Long.parseLong(br.readLine().trim());
            long a[] = new long[(int)(n)];
            String inputLine[] = br.readLine().trim().split(" ");
            for (int i = 0; i < n; i++) {
                a[i] = Long.parseLong(inputLine[i]);
            }
            
            Compute obj = new Compute();
            long[] product = obj.minAnd2ndMin(a, n); 
            if(product[0]==-1)
                System.out.println(product[0]);
            else
                System.out.println(product[0]+" "+product[1]);
            
        }
	}
}



//User function Template for

class Compute {
    public long[] minAnd2ndMin(long a[], long n) {
        Arrays.sort(a);
        long min = 0;
        long min_sec = 0;
        min = a[0];
        for (int i = 0; i < n - 1; i++) {
            if (a[i] != a[i + 1]) {
                min_sec = a[i + 1];
                break;
            }
        }
        if (min_sec == 0) {
            return new long[] { -1 };
        }
        long[] arr = { min, min_sec };
        return arr;
    }
}