//{ Driver Code Starts
//Initial Template for Java



import java.util.*;
import java.io.*;

public class Main {

    public static void main(String[] args) throws Exception {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        int tc = Integer.parseInt(br.readLine().trim());
        while (tc-- > 0) {
            String[] inputLine;
            int n = Integer.parseInt(br.readLine().trim());
            int[] arr = new int[n];
            inputLine = br.readLine().trim().split(" ");
            for (int i = 0; i < n; i++) {
                arr[i] = Integer.parseInt(inputLine[i]);
            }

            new Solution().rearrange(arr, n);
            for (int i = 0; i < n; i++) {
                System.out.print(arr[i] + " ");
            }
            System.out.println();
        }
    }
}

// } Driver Code Ends


//User function Template for Java




class Solution {
    void rearrange(int arr[], int n) {
        int[] positiveNumbers = new int[n];
        int[] negativeNumbers = new int[n];

        int posCount = 0;
        int negCount = 0;

        for (int num : arr) {
            if (num >= 0) {
                positiveNumbers[posCount++] = num;
            } else {
                negativeNumbers[negCount++] = num;
            }
        }

        int i = 0, j = 0, k = 0;

        while (i < posCount && j < negCount) {
            arr[k++] = positiveNumbers[i++];
            arr[k++] = negativeNumbers[j++];
        }
        while (i < posCount) {
            arr[k++] = positiveNumbers[i++];
        }
        while (j < negCount) {
            arr[k++] = negativeNumbers[j++];
        }
    }
}