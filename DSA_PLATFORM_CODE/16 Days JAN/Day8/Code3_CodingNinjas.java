import java.util.* ;
import java.io.*; 

public class Solution {
	public static void posAndNeg(int[] arr) {
		int n=arr.length;
		int[] positiveNumbers = new int[n];
        int[] negativeNumbers = new int[n];

        int posCount = 0;
        int negCount = 0;

        for (int num : arr) {
            if (num >= 0) {
                positiveNumbers[posCount++] = num;
            } else {
                negativeNumbers[negCount++] = num;
            }
        }

        int i = 0, j = 0, k = 0;

        while (i < posCount && j < negCount) {
            arr[k++] = positiveNumbers[i++];
            arr[k++] = negativeNumbers[j++];
        }
        while (i < posCount) {
            arr[k++] = positiveNumbers[i++];
        }
        while (j < negCount) {
            arr[k++] = negativeNumbers[j++];
        }
	}

}